﻿namespace TextGridFormFrontend
{
    partial class frmMapGen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMapGen));
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.nadWidth = new System.Windows.Forms.NumericUpDown();
            this.nadHeight = new System.Windows.Forms.NumericUpDown();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.pbxMapDisplay = new System.Windows.Forms.PictureBox();
            this.lblSeed = new System.Windows.Forms.Label();
            this.nadSeed = new System.Windows.Forms.NumericUpDown();
            this.lblZoom = new System.Windows.Forms.Label();
            this.lblDetail = new System.Windows.Forms.Label();
            this.nadZoom = new System.Windows.Forms.NumericUpDown();
            this.nadDetail = new System.Windows.Forms.NumericUpDown();
            this.lblLacunarity = new System.Windows.Forms.Label();
            this.lblGain = new System.Windows.Forms.Label();
            this.nadGain = new System.Windows.Forms.NumericUpDown();
            this.nadLacunarity = new System.Windows.Forms.NumericUpDown();
            this.sfdSaveMap = new System.Windows.Forms.SaveFileDialog();
            this.btnSave = new System.Windows.Forms.Button();
            this.pbrGeneration = new System.Windows.Forms.ProgressBar();
            this.lblStartX = new System.Windows.Forms.Label();
            this.lblStartY = new System.Windows.Forms.Label();
            this.nadStartX = new System.Windows.Forms.NumericUpDown();
            this.nadStartY = new System.Windows.Forms.NumericUpDown();
            this.mnsMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofdLoadRegion = new System.Windows.Forms.OpenFileDialog();
            this.sfdSaveRegion = new System.Windows.Forms.SaveFileDialog();
            this.nadSeaLevel = new System.Windows.Forms.NumericUpDown();
            this.lblSeaLevel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nadWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMapDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadSeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadGain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadLacunarity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadStartX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadStartY)).BeginInit();
            this.mnsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nadSeaLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // lblWidth
            // 
            this.lblWidth.AutoSize = true;
            this.lblWidth.Location = new System.Drawing.Point(12, 32);
            this.lblWidth.Margin = new System.Windows.Forms.Padding(3);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(35, 13);
            this.lblWidth.TabIndex = 2;
            this.lblWidth.Text = "Width";
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Location = new System.Drawing.Point(125, 32);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(38, 13);
            this.lblHeight.TabIndex = 3;
            this.lblHeight.Text = "Height";
            // 
            // nadWidth
            // 
            this.nadWidth.Location = new System.Drawing.Point(53, 28);
            this.nadWidth.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.nadWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nadWidth.Name = "nadWidth";
            this.nadWidth.Size = new System.Drawing.Size(66, 20);
            this.nadWidth.TabIndex = 4;
            this.nadWidth.Value = new decimal(new int[] {
            800,
            0,
            0,
            0});
            // 
            // nadHeight
            // 
            this.nadHeight.Location = new System.Drawing.Point(169, 28);
            this.nadHeight.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.nadHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nadHeight.Name = "nadHeight";
            this.nadHeight.Size = new System.Drawing.Size(66, 20);
            this.nadHeight.TabIndex = 5;
            this.nadHeight.Value = new decimal(new int[] {
            460,
            0,
            0,
            0});
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerate.Location = new System.Drawing.Point(737, 27);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 6;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // pbxMapDisplay
            // 
            this.pbxMapDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxMapDisplay.Location = new System.Drawing.Point(12, 90);
            this.pbxMapDisplay.Name = "pbxMapDisplay";
            this.pbxMapDisplay.Size = new System.Drawing.Size(800, 460);
            this.pbxMapDisplay.TabIndex = 7;
            this.pbxMapDisplay.TabStop = false;
            // 
            // lblSeed
            // 
            this.lblSeed.AutoSize = true;
            this.lblSeed.Location = new System.Drawing.Point(241, 32);
            this.lblSeed.Name = "lblSeed";
            this.lblSeed.Size = new System.Drawing.Size(32, 13);
            this.lblSeed.TabIndex = 8;
            this.lblSeed.Text = "Seed";
            // 
            // nadSeed
            // 
            this.nadSeed.Location = new System.Drawing.Point(279, 28);
            this.nadSeed.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nadSeed.Name = "nadSeed";
            this.nadSeed.Size = new System.Drawing.Size(176, 20);
            this.nadSeed.TabIndex = 9;
            // 
            // lblZoom
            // 
            this.lblZoom.AutoSize = true;
            this.lblZoom.Location = new System.Drawing.Point(12, 60);
            this.lblZoom.Name = "lblZoom";
            this.lblZoom.Size = new System.Drawing.Size(34, 13);
            this.lblZoom.TabIndex = 10;
            this.lblZoom.Text = "Zoom";
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = true;
            this.lblDetail.Location = new System.Drawing.Point(125, 60);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(34, 13);
            this.lblDetail.TabIndex = 11;
            this.lblDetail.Text = "Detail";
            // 
            // nadZoom
            // 
            this.nadZoom.Location = new System.Drawing.Point(53, 56);
            this.nadZoom.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nadZoom.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nadZoom.Name = "nadZoom";
            this.nadZoom.Size = new System.Drawing.Size(66, 20);
            this.nadZoom.TabIndex = 12;
            this.nadZoom.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // nadDetail
            // 
            this.nadDetail.Location = new System.Drawing.Point(169, 56);
            this.nadDetail.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nadDetail.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nadDetail.Name = "nadDetail";
            this.nadDetail.Size = new System.Drawing.Size(66, 20);
            this.nadDetail.TabIndex = 13;
            this.nadDetail.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // lblLacunarity
            // 
            this.lblLacunarity.AutoSize = true;
            this.lblLacunarity.Location = new System.Drawing.Point(335, 60);
            this.lblLacunarity.Name = "lblLacunarity";
            this.lblLacunarity.Size = new System.Drawing.Size(56, 13);
            this.lblLacunarity.TabIndex = 14;
            this.lblLacunarity.Text = "Lacunarity";
            // 
            // lblGain
            // 
            this.lblGain.AutoSize = true;
            this.lblGain.Location = new System.Drawing.Point(241, 60);
            this.lblGain.Name = "lblGain";
            this.lblGain.Size = new System.Drawing.Size(29, 13);
            this.lblGain.TabIndex = 15;
            this.lblGain.Text = "Gain";
            // 
            // nadGain
            // 
            this.nadGain.DecimalPlaces = 2;
            this.nadGain.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nadGain.Location = new System.Drawing.Point(279, 56);
            this.nadGain.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nadGain.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.nadGain.Name = "nadGain";
            this.nadGain.Size = new System.Drawing.Size(50, 20);
            this.nadGain.TabIndex = 16;
            this.nadGain.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // nadLacunarity
            // 
            this.nadLacunarity.DecimalPlaces = 2;
            this.nadLacunarity.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nadLacunarity.Location = new System.Drawing.Point(395, 56);
            this.nadLacunarity.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nadLacunarity.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.nadLacunarity.Name = "nadLacunarity";
            this.nadLacunarity.Size = new System.Drawing.Size(60, 20);
            this.nadLacunarity.TabIndex = 17;
            this.nadLacunarity.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // sfdSaveMap
            // 
            this.sfdSaveMap.DefaultExt = "bmp";
            this.sfdSaveMap.FileName = "map.bmp";
            this.sfdSaveMap.Filter = "Bitmap files|*.bmp";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(737, 56);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "Save As...";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pbrGeneration
            // 
            this.pbrGeneration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbrGeneration.Location = new System.Drawing.Point(631, 27);
            this.pbrGeneration.Name = "pbrGeneration";
            this.pbrGeneration.Size = new System.Drawing.Size(100, 23);
            this.pbrGeneration.Step = 20;
            this.pbrGeneration.TabIndex = 20;
            // 
            // lblStartX
            // 
            this.lblStartX.AutoSize = true;
            this.lblStartX.Location = new System.Drawing.Point(461, 32);
            this.lblStartX.Name = "lblStartX";
            this.lblStartX.Size = new System.Drawing.Size(53, 13);
            this.lblStartX.TabIndex = 21;
            this.lblStartX.Text = "Starting X";
            // 
            // lblStartY
            // 
            this.lblStartY.AutoSize = true;
            this.lblStartY.Location = new System.Drawing.Point(461, 60);
            this.lblStartY.Name = "lblStartY";
            this.lblStartY.Size = new System.Drawing.Size(53, 13);
            this.lblStartY.TabIndex = 22;
            this.lblStartY.Text = "Starting Y";
            // 
            // nadStartX
            // 
            this.nadStartX.Location = new System.Drawing.Point(519, 28);
            this.nadStartX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nadStartX.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.nadStartX.Name = "nadStartX";
            this.nadStartX.Size = new System.Drawing.Size(66, 20);
            this.nadStartX.TabIndex = 23;
            // 
            // nadStartY
            // 
            this.nadStartY.Location = new System.Drawing.Point(519, 56);
            this.nadStartY.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nadStartY.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.nadStartY.Name = "nadStartY";
            this.nadStartY.Size = new System.Drawing.Size(66, 20);
            this.nadStartY.TabIndex = 24;
            // 
            // mnsMain
            // 
            this.mnsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mnsMain.Location = new System.Drawing.Point(0, 0);
            this.mnsMain.Name = "mnsMain";
            this.mnsMain.Size = new System.Drawing.Size(824, 24);
            this.mnsMain.TabIndex = 25;
            this.mnsMain.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.newToolStripMenuItem.Text = "&New";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(143, 6);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Export";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(119, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            // 
            // ofdLoadRegion
            // 
            this.ofdLoadRegion.Filter = "JSON files|*.json";
            // 
            // sfdSaveRegion
            // 
            this.sfdSaveRegion.DefaultExt = "json";
            this.sfdSaveRegion.FileName = "region.json";
            this.sfdSaveRegion.Filter = "JSON files|*.json";
            // 
            // nadSeaLevel
            // 
            this.nadSeaLevel.DecimalPlaces = 4;
            this.nadSeaLevel.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nadSeaLevel.Location = new System.Drawing.Point(664, 56);
            this.nadSeaLevel.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            65536});
            this.nadSeaLevel.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.nadSeaLevel.Name = "nadSeaLevel";
            this.nadSeaLevel.Size = new System.Drawing.Size(66, 20);
            this.nadSeaLevel.TabIndex = 26;
            this.nadSeaLevel.Value = new decimal(new int[] {
            4,
            0,
            0,
            65536});
            // 
            // lblSeaLevel
            // 
            this.lblSeaLevel.AutoSize = true;
            this.lblSeaLevel.Location = new System.Drawing.Point(603, 60);
            this.lblSeaLevel.Name = "lblSeaLevel";
            this.lblSeaLevel.Size = new System.Drawing.Size(55, 13);
            this.lblSeaLevel.TabIndex = 27;
            this.lblSeaLevel.Text = "Sea Level";
            // 
            // frmMapGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 562);
            this.Controls.Add(this.lblSeaLevel);
            this.Controls.Add(this.nadSeaLevel);
            this.Controls.Add(this.nadStartY);
            this.Controls.Add(this.nadStartX);
            this.Controls.Add(this.lblStartY);
            this.Controls.Add(this.lblStartX);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.nadWidth);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.nadHeight);
            this.Controls.Add(this.pbrGeneration);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.nadLacunarity);
            this.Controls.Add(this.nadGain);
            this.Controls.Add(this.lblGain);
            this.Controls.Add(this.lblLacunarity);
            this.Controls.Add(this.nadDetail);
            this.Controls.Add(this.nadZoom);
            this.Controls.Add(this.lblDetail);
            this.Controls.Add(this.lblZoom);
            this.Controls.Add(this.nadSeed);
            this.Controls.Add(this.lblSeed);
            this.Controls.Add(this.pbxMapDisplay);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.mnsMain);
            this.MainMenuStrip = this.mnsMain;
            this.Name = "frmMapGen";
            this.Text = "MapGen";
            ((System.ComponentModel.ISupportInitialize)(this.nadWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMapDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadSeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadGain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadLacunarity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadStartX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadStartY)).EndInit();
            this.mnsMain.ResumeLayout(false);
            this.mnsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nadSeaLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.NumericUpDown nadWidth;
        private System.Windows.Forms.NumericUpDown nadHeight;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.PictureBox pbxMapDisplay;
        private System.Windows.Forms.Label lblSeed;
        private System.Windows.Forms.NumericUpDown nadSeed;
        private System.Windows.Forms.Label lblZoom;
        private System.Windows.Forms.Label lblDetail;
        private System.Windows.Forms.NumericUpDown nadZoom;
        private System.Windows.Forms.NumericUpDown nadDetail;
        private System.Windows.Forms.Label lblLacunarity;
        private System.Windows.Forms.Label lblGain;
        private System.Windows.Forms.NumericUpDown nadGain;
        private System.Windows.Forms.NumericUpDown nadLacunarity;
        private System.Windows.Forms.SaveFileDialog sfdSaveMap;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ProgressBar pbrGeneration;
        private System.Windows.Forms.Label lblStartX;
        private System.Windows.Forms.Label lblStartY;
        private System.Windows.Forms.NumericUpDown nadStartX;
        private System.Windows.Forms.NumericUpDown nadStartY;
        private System.Windows.Forms.MenuStrip mnsMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog ofdLoadRegion;
        private System.Windows.Forms.SaveFileDialog sfdSaveRegion;
        private System.Windows.Forms.NumericUpDown nadSeaLevel;
        private System.Windows.Forms.Label lblSeaLevel;
    }
}

