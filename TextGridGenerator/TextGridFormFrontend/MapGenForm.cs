﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MapGen.Entities.Grid;
using MapGen.Generators;
using MapGen.Renderers;
using MapGen.Storage;

namespace TextGridFormFrontend
{
    public partial class frmMapGen : Form
    {
        ImageRenderer ir;
        MapGen.Entities.Grid.Region r;

        public frmMapGen()
        {
            InitializeComponent();
            ir = new ImageRenderer();
            r = new MapGen.Entities.Grid.Region();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            pbrGeneration.Value = 0;
            btnSave.Enabled = false;
            pbrGeneration.PerformStep();
            SimplexGridGenerator sg = new SimplexGridGenerator((int)nadSeed.Value, (int)nadDetail.Value, (int)nadZoom.Value, (float)nadGain.Value, (float)nadLacunarity.Value);
            pbrGeneration.PerformStep();
            r.elevations = sg.GenerateNoiseGrid((int)nadWidth.Value, (int)nadHeight.Value, (int)nadStartX.Value, (int)nadStartY.Value);
            r.moisture = sg.GenerateNoiseGrid((int)nadWidth.Value, (int)nadHeight.Value, (int)nadStartX.Value, (int)nadStartY.Value);
            float max = r.elevations.Cast<float>().Max(); // Debug code
            float min = r.elevations.Cast<float>().Min(); // Debug code
            pbrGeneration.PerformStep();
            displayCurrentGrid();
            pbrGeneration.PerformStep();
            btnSave.Enabled = true;
            pbrGeneration.PerformStep();
        }

        private void displayCurrentGrid()
        {
            pbxMapDisplay.Image = ir.RenderToBitmap(r, (float)nadSeaLevel.Value);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            nadWidth.Value = pbxMapDisplay.Width;
            nadHeight.Value = pbxMapDisplay.Height;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult d = sfdSaveMap.ShowDialog();

            if (d == DialogResult.OK)
            {
                pbxMapDisplay.Image.Save(sfdSaveMap.FileName);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = ofdLoadRegion.ShowDialog();
            if (dr == DialogResult.OK)
            {
                RegionSerializer rs = new RegionSerializer();
                r = rs.Deserialise(ofdLoadRegion.FileName);
                displayCurrentGrid();
                btnSave.Enabled = true;
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = sfdSaveRegion.ShowDialog();
            if (dr == DialogResult.OK)
            {
                RegionSerializer rs = new RegionSerializer();
                rs.Serialize(sfdSaveRegion.FileName, r);
                MessageBox.Show("Saved successfully.");
            }
        }
    }
}
