﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen.Entities.Biomes
{
    public abstract class Biome
    {
        public abstract Color biomeColor { get; set; }
    }
}
