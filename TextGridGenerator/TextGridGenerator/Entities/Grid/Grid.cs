﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapGen.Entities.Tiles;

namespace MapGen.Entities.Grid
{
    public class Grid
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public Tile[,] Tiles { get; set; }

        public Grid(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.Tiles = new Tile[width, height];
        }
    }
}
