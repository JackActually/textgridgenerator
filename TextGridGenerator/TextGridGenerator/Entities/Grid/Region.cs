﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen.Entities.Grid
{
    public class Region
    {
        public Region() { }
        public float[,] elevations { get; set; }
        public float[,] moisture { get; set; }
    }
}
