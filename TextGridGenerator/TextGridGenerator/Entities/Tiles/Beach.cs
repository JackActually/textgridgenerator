﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class Beach : Tile
    {
        private string _icon = ".";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.Yellow;
        public override Color ImageColour { get { return _imageColour; } }

        public Beach(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.WaterShallow, 30),
                new TileRelationship(TileType.Beach, 40),
                new TileRelationship(TileType.Cliff, 5),
                new TileRelationship(TileType.Hill, 5),
                new TileRelationship(TileType.Inland, 20)
            };
            this.Background = ConsoleColor.Yellow;
            this.Foreground = ConsoleColor.Black;
        }
    }
}
