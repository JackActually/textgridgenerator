﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class Cliff : Tile
    {
        private string _icon = "|";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.LightGray;
        public override Color ImageColour { get { return _imageColour; } }

        public Cliff(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.WaterShallow, 30),
                new TileRelationship(TileType.Beach, 30),
                new TileRelationship(TileType.Cliff, 50),
                new TileRelationship(TileType.Mountain, 10),
                new TileRelationship(TileType.Hill, 50),
                new TileRelationship(TileType.Inland, 50)
            };
            this.Background = ConsoleColor.DarkGray;
        }
    }
}
