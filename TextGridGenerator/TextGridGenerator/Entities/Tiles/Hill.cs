﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class Hill : Tile
    {
        private string _icon = "∩";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.DarkGreen;
        public override Color ImageColour { get { return _imageColour; } }

        public Hill(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.Beach, 20),
                new TileRelationship(TileType.Cliff, 20),
                new TileRelationship(TileType.Mountain, 30),
                new TileRelationship(TileType.Hill, 50),
                new TileRelationship(TileType.Inland, 60)
            };
            this.Background = ConsoleColor.DarkGreen;
        }
    }
}
