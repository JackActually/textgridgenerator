﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class Inland : Tile
    {
        private string _icon = ":";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.LightGreen;
        public override Color ImageColour { get { return _imageColour; } }

        public Inland(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.Beach, 20),
                new TileRelationship(TileType.Cliff, 10),
                new TileRelationship(TileType.Hill, 30),
                new TileRelationship(TileType.Inland, 60)
            };
            this.Background = ConsoleColor.DarkGreen;
        }
    }
}
