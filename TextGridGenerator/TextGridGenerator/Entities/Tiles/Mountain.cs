﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class Mountain : Tile
    {
        private string _icon = "^";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.DarkGray;
        public override Color ImageColour { get { return _imageColour; } }

        public Mountain(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.Cliff, 20),
                new TileRelationship(TileType.Mountain, 40),
                new TileRelationship(TileType.Hill, 50),
            };
            this.Background = ConsoleColor.DarkGray;
        }
    }
}
