﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class Source : Tile
    {
        private string _icon = "?";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.White;
        public override Color ImageColour { get { return _imageColour; } }

        public Source(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.Beach, 30),
                new TileRelationship(TileType.Cliff, 30),
                new TileRelationship(TileType.Hill, 30),
                new TileRelationship(TileType.Inland, 30),
                new TileRelationship(TileType.Mountain, 30),
                new TileRelationship(TileType.WaterDeep, 30),
                new TileRelationship(TileType.WaterShallow, 30)
            };
            this.Background = ConsoleColor.Red;
            this.Foreground = ConsoleColor.Black;
        }
    }
}
