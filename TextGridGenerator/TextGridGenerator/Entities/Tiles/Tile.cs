﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public abstract class Tile
    {
        public Tile(int id)
        {
            this.Id = id;
            this.Background = ConsoleColor.Black;
            this.Foreground = ConsoleColor.White;
        }

        public Tile(int id, ConsoleColor background)
        {
            this.Id = id;
            this.Background = background;
            this.Foreground = ConsoleColor.White;
        }

        public Tile(int id, ConsoleColor background, ConsoleColor foreground)
        {
            this.Id = id;
            this.Background = background;
            this.Foreground = foreground;
        }

        public int Id { get; set; }
        // Background, Foreground and possibly Icon properties should be removed when this is converted into a non-console app.
        public ConsoleColor Background { get; set; }
        public ConsoleColor Foreground { get; set; }
        public abstract Color ImageColour { get; }
        public abstract string Icon { get; }
        public List<TileRelationship> Relationships { get; set; }
    }

    public enum TileType { Beach, Cliff, Hill, Inland, Mountain, WaterDeep, WaterShallow }
}
