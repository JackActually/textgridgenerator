﻿using System.Collections.Generic;

namespace MapGen.Entities.Tiles
{
    public class TileRelationship
    {
        public TileRelationship(TileType tileType, int affinity)
        {
            this.TileType = tileType;
            this.Affinity = affinity;
        }

        public TileType TileType { get; set; }
        public int Affinity { get; set; }
    }

    public class TileComparer : IEqualityComparer<TileRelationship>
    {
        public bool Equals(TileRelationship x, TileRelationship y)
        {
            return x.TileType == y.TileType;
        }

        public int GetHashCode(TileRelationship tr)
        {
            return tr.TileType.GetHashCode();
        }
    }
}
