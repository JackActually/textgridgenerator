﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class WaterDeep : Tile
    {
        private string _icon = "~";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.DarkBlue;
        public override Color ImageColour { get { return _imageColour; } }

        public WaterDeep(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.WaterShallow, 20),
                new TileRelationship(TileType.WaterDeep, 80)
            };
            this.Background = ConsoleColor.DarkBlue;
        }
    }
}
