﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MapGen.Entities.Tiles
{
    public class WaterShallow : Tile
    {
        private string _icon = "~";
        public override string Icon { get { return _icon; } }

        private Color _imageColour = Color.Blue;
        public override Color ImageColour { get { return _imageColour; } }

        public WaterShallow(int id)
            : base(id)
        {
            this.Relationships = new List<TileRelationship>{
                new TileRelationship(TileType.WaterShallow, 70),
                new TileRelationship(TileType.Beach, 10),
                new TileRelationship(TileType.Cliff, 5),
                new TileRelationship(TileType.WaterDeep, 15)
            };
            this.Background = ConsoleColor.DarkCyan;
        }
    }
}
