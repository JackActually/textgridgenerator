﻿using MapGen.Entities.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen.Generators
{
    public interface IGridGenerator
    {
        Grid GenerateGrid(int width, int height);
    }
}
