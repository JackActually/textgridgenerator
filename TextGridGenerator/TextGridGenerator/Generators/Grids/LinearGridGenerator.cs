﻿using MapGen.Entities.Grid;
using MapGen.Entities.Tiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen.Generators
{
    public class LinearGridGenerator : IGridGenerator
    {
        private Grid _grid;
        public Grid GenerateGrid(int width, int height)
        {
            RelationshipTileGenerator tm = new RelationshipTileGenerator();
            Tile tileLeft = new Source(-1);
            Tile tileAbove = new Source(-1);
            _grid = new Grid(width, height);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    _grid.Tiles[j, i] = tm.GetNextTile(new List<Tile>{tileLeft, tileAbove});
                    tileLeft = _grid.Tiles[j, i];
                    if (i > 0)
                        tileAbove = _grid.Tiles[j, i - 1];
                }
                tileLeft = new Source(-1);
                tileAbove = _grid.Tiles[0, i];
            }
            return _grid;
        }
    }
}
