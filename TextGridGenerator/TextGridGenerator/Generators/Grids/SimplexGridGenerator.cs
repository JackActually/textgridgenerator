﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapGen.Entities.Grid;
using MapGen.Entities.Tiles;
using SimplexNoise;

namespace MapGen.Generators
{
    public class SimplexGridGenerator : IGridGenerator
    {
        private Grid _grid;
        private Random _rnd;
        private int _detail = 16;
        private int _zoom = 1000;
        private float _gain = 0.5f;
        private float _lacunarity = 2.0f;

        public SimplexGridGenerator()
        {
            _rnd = new Random();

        }
        public SimplexGridGenerator(int seed)
        {
            _rnd = new Random(seed);
        }

        public SimplexGridGenerator(int seed, int detail, int zoom, float gain, float lacunarity)
        {
            _rnd = new Random(seed);
            _detail = detail;
            _zoom = zoom;
            _gain = gain;
            _lacunarity = lacunarity;
        }

        public Grid GenerateGrid(int width, int height)
        {
            _grid = new Grid(width, height);
            byte[] noiseSeed = new byte[512];
            _rnd.NextBytes(noiseSeed);
            Noise.perm = noiseSeed;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float noise = 0 + fractionalBrownianMomentPeturbation(x, y, _detail, _lacunarity, _gain, 1f / _zoom);
                    _grid.Tiles[x, y] = elevationToTile(noise);
                }
            }
            return _grid;
        }

        // Noise array needs to be symmetrical in some ways.
        private byte[] generateNoiseSeed()
        {
            byte[] noiseSeed = new byte[256];
            _rnd.NextBytes(noiseSeed);
            List<byte> combinedNoise = new List<byte>();
            combinedNoise.AddRange(noiseSeed);
            combinedNoise.AddRange(noiseSeed);
            return combinedNoise.ToArray();
        }

        public float[,] GenerateNoiseGrid(int width, int height)
        {
            return GenerateNoiseGrid(width, height, 0, 0);
        }

        public float[,] GenerateNoiseGrid(int width, int height, int startX, int startY)
        {
            float[,] noiseGrid = new float[width, height];
            int offsetX = startX;
            int offsetY = startY;
            Noise.perm = generateNoiseSeed();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float noise = fractionalBrownianMomentPeturbationEroded(offsetX, offsetY, _detail, _lacunarity, _gain, 1f / _zoom);
                    noiseGrid[x, y] = noise;
                    offsetX++;
                }
                offsetX = startX;
                offsetY++;
            }
            return noiseGrid;
        }

        public float[,] GenerateTestGrid()
        {
            int dim = 1000;
            float[,] noiseGrid = new float[dim, dim];
            Noise.perm = generateNoiseSeed();
            for (int y = 0; y < dim; y++)
            {
                for (int x = 0; x < dim; x++)
                {
                    noiseGrid[x, y] = Noise.Generate(x, y);
                }
            }
            return noiseGrid;
        }

        // Should increase number of octaves as frequency increases - increase detail level with 'zoom'. (More octaves, more detail. More frequency, more zoon.
        // Code cribbed from http://gamedev.stackexchange.com/questions/58664/2d-and-3d-perlin-noise-terrain-generation
        private float fractionalBrownianMomentPeturbation(float x, float y, int octaves = 8, float lacunarity = 2.0f, float gain = 0.5f, float frequency = 0.001f)
        {
            float amplitude = 1.0f;
            float sum = 0.0f;
            for (int i = 0; i < octaves; i++)
            {
                sum += amplitude * Noise.Generate(x * frequency, y * frequency);
                amplitude *= gain;
                frequency *= lacunarity;
            }
            return sum;
        }

        private float fractionalBrownianMomentPeturbationEroded(float x, float y, int octaves = 8, float lacunarity = 2.0f, float gain = 0.5f, float frequency = 0.001f)
        {
            float amplitude = 1.0f;
            float sum = 0.0f;
            for (int i = 0; i < octaves; i++)
            {
                sum += amplitude * (Math.Abs(Noise.Generate(x * frequency, y * frequency)));
                amplitude *= gain;
                frequency *= lacunarity;
            }
            return sum;
        }

        private Tile elevationToTile(float elevation)
        {
            if (elevation < 0)
            {
                return new WaterDeep(-1);
            }
            else if (elevation < 0.4)
            {
                return new WaterShallow(-1);
            }
            else if (elevation < 0.5)
            {
                return new Beach(-1);
            }
            else if (elevation < 1)
            {
                return new Inland(-1);
            }
            else if (elevation < 1.2)
            {
                return new Hill(-1);
            }
            else
            {
                return new Mountain(-1);
            }
        }

        // Odds and ends from attempts to implement my own noise functions.
        # region Obsolete stuff
        // Methods cribbed from http://freespace.virgin.net/hugo.elias/models/m_perlin.htm
        // a, b: values to be interpolated between
        // x: value between 0 and 1.
        private double linearInterpolate(int a, int b, double x)
        {
            return a * (1 - x) + b * x;
        }

        private double CosineInterpolate(int a, int b, double x)
        {
            double ft = x * 3.1415927;
            double f = (1 - Math.Cos(ft)) * 0.5;
            return a*(1-f) + b*f;
        }

        private double interpolatedNoise(double inputRand)
        {
            int inputRandAsInt = (int)inputRand;
            double fracInput = inputRand - inputRandAsInt;

            int from = inputRandAsInt;
            int to = inputRandAsInt + 1;

            return CosineInterpolate(from, to, fracInput);
        }

        private int numOctaves = 4;
        // inputRand: between 0 and 1
        private double PerlinNoise_1D(double inputRand)
        {
            double total = 0;
            double persistence = 1;

            for (int i = 0; i < numOctaves; i++)
            {
                int frequency = 2 ^ i;
                double amplitude = Math.Pow(persistence, i);
                total += interpolatedNoise(inputRand * frequency) * amplitude;
            }
            return total;
        }

        private double NoiseGen2D(int x, int y){
            int n = x + y * 57;
            n = (n << 13) ^ n;
            return (1.0 - ((n*(n*n*15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
        }
        #endregion
    }
}
