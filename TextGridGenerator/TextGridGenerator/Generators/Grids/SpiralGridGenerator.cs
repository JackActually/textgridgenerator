﻿using MapGen.Entities.Grid;
using MapGen.Entities.Tiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen.Generators
{
    public class SpiralGridGenerator : IGridGenerator
    {
        private Random rnd;
        private Grid _grid;
        public SpiralGridGenerator()
        {
            rnd = new Random();
        }

        // Major hat tip to Nikita Rybak of SO for the basis of this method.
        // http://stackoverflow.com/questions/3706219/algorithm-for-iterating-over-an-outward-spiral-on-a-discrete-2d-grid-from-the-or
        public Grid GenerateGrid(int width, int height)
        {
            int totalPoints = width * height;
            int totalPointsActual = width * height;
            int longestSide = getHigherValue(width, height);
            // This could be more efficient - could base projected number of points on generated centre points.
            // Value multiplied by four to account for the possibility of origin points of [0,0], [xMax,0], [0, yMax], [xMax, yMax]
            int totalPointsProjected = (longestSide * longestSide) * 4;

            _grid = new Grid(width, height);
            RelationshipTileGenerator tm = new RelationshipTileGenerator();
            // (di, dj) is a vector - direction in which we move right now
            int di = 1;
            int dj = 0;
            // length of current segment
            int segment_length = 1;

            // current position (i, j) and how much of current segment we passed
            int i = rnd.Next(width);
            int j = rnd.Next(height);
            int segment_passed = 0;
            for (int k = 0; k < totalPointsProjected; ++k)
            {
                // If the points aren't within the boundaries of the grid, we do nothing with them - would have array Problems otherwise.
                if (pointsWithinBounds(i, j, width, height))
                {
                    _grid.Tiles[i, j] = tm.GetNextTile(getSurroundingTiles(i, j, width, height));
                }
                // make a step, add 'direction' vector (di, dj) to current position (i, j)
                i += di;
                j += dj;
                ++segment_passed;

                if (segment_passed == segment_length)
                {
                    // done with current segment
                    segment_passed = 0;

                    // 'rotate' directions
                    int buffer = di;
                    di = -dj;
                    dj = buffer;

                    // increase segment length if necessary
                    if (dj == 0)
                    {
                        ++segment_length;
                    }
                }
            }
            return _grid;
        }

        private int getHigherValue(int left, int right)
        {
            return left > right ? left : right;
        }

        private bool pointsWithinBounds(int xPoint, int yPoint, int width, int height)
        {
            return ((xPoint >= 0 && xPoint < width) && (yPoint >= 0 && yPoint < height));
        }

        private List<Tile> getSurroundingTiles(int x, int y, int width, int height)
        {
            // TODO: Super hacky method for now. Improve the crap out of this.
            int[][] pointsToCheck = { new int[] { x - 1, y - 1 }, new int[] { x, y - 1 }, new int[] { x + 1, y - 1 }, new int[] { x - 1, y }, new int[] { x + 1, y }, new int[] { x - 1, y + 1 }, new int[] { x, y + 1 }, new int[] { x + 1, y + 1 } };
            List<Tile> surroundingTiles = new List<Tile>();
            foreach (int[] points in pointsToCheck)
            {
                if ((points[0] >= 0 && points[0] < width) && (points[1] >= 0 && points[1] < height)) // Should check this before adding to pointsToCheck...
                {
                    if (_grid.Tiles[points[0], points[1]] != null)
                    {
                        surroundingTiles.Add(_grid.Tiles[points[0], points[1]]);
                    }
                    else
                    {
                        surroundingTiles.Add(new Source(-1));
                    }
                }
            }
            return surroundingTiles;
        }

        private int getCentrePoint(double sideLength)
        {
            return (int)Math.Floor(sideLength / 2);
        }
    }
}
