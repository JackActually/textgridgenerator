﻿using MapGen.Entities.Tiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen.Generators
{
    public interface ITileGenerator
    {
        Tile GetNextTile(Tile tile);
        Tile GetNextTile(IEnumerable<Tile> tiles);
    }
}
