﻿using MapGen.Entities.Tiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen.Generators
{
    public class RelationshipTileGenerator : ITileGenerator
    {
        private Random _rnd;
        private Tile _lastTile;
        private int _idCounter;
        public RelationshipTileGenerator()
        {
            _rnd = new Random();
            _idCounter = 0;
            _lastTile = new Source(_idCounter);
            _idCounter++;
        }

        // Returns a new tile based on the relationships of the tile passed to it.
        public Tile GetNextTile(Tile tile)
        {
            return getTile(tile.Relationships);
        }

        // Returns a new tile based on the relationships of the collection of tiles passed to it.
        public Tile GetNextTile(IEnumerable<Tile> tiles)
        {
            List<Tile> adjacentTiles = tiles.ToList<Tile>();
            if (adjacentTiles.Count <= 1)
                return getTile(_lastTile.Relationships);
            List<TileRelationship> mergedRelationships = mergeRelationships(adjacentTiles);
            return getTile(mergedRelationships);
        }

        private Tile getTile(List<TileRelationship> relationships)
        {
            int totalAffinity = sumAffinity(relationships);
            int randomNumber = _rnd.Next(0, totalAffinity);
            Tile selected = null;
            foreach (TileRelationship tr in relationships)
            {
                if (randomNumber < tr.Affinity)
                {
                    selected = createTile(tr.TileType);
                    break;
                }
                randomNumber = randomNumber - tr.Affinity;
            }
            _lastTile = selected;
            return selected;
        }

        private List<TileRelationship> mergeRelationships(List<Tile> tilesToMerge)
        {
            List<TileRelationship> mergedRelationships = tilesToMerge[0].Relationships;
            tilesToMerge.RemoveAt(0);
            foreach (Tile t in tilesToMerge)
            {
                List<TileRelationship> scratchList = new List<TileRelationship>();
                var query = from rel1 in mergedRelationships
                            join rel2 in t.Relationships on rel1.TileType equals rel2.TileType
                            select new { mergedType = rel1.TileType, mergedAffinity = (rel1.Affinity + rel2.Affinity) };
                foreach (var mergedRelationship in query)
                {
                    scratchList.Add(new TileRelationship(mergedRelationship.mergedType, mergedRelationship.mergedAffinity));
                }
                mergedRelationships = scratchList;
            }
            return mergedRelationships;
        }

        private int sumAffinity(List<TileRelationship> relationships)
        {
            return relationships.Sum(x => x.Affinity);
        }

        // TODO: refactor this. This (probably) isn't sustainable.
        private Tile createTile(TileType type)
        {
            Tile newTile = null;
            switch (type)
            {
                case TileType.Beach:
                    newTile = new Beach(_idCounter);
                    break;
                case TileType.Cliff:
                    newTile = new Cliff(_idCounter);
                    break;
                case TileType.Hill:
                    newTile = new Hill(_idCounter);
                    break;
                case TileType.Inland:
                    newTile = new Inland(_idCounter);
                    break;
                case TileType.Mountain:
                    newTile = new Mountain(_idCounter);
                    break;
                case TileType.WaterDeep:
                    newTile = new WaterDeep(_idCounter);
                    break;
                case TileType.WaterShallow:
                    newTile = new WaterShallow(_idCounter);
                    break;
                default:
                    break;
            }
            _idCounter++;
            return newTile;
        }
    }
}
