﻿using System;
using MapGen.Entities.Grid;
using MapGen.Entities.Tiles;

namespace MapGen.Renderers
{
    public class ConsoleRenderer
    {
        public void RenderGrid(Grid g)
        {
            for (int i = 0; i < g.Height; i++)
            {
                for (int j = 0; j < g.Width; j++)
                {
                    renderTile(g.Tiles[j, i]);
                }
                Console.WriteLine(); // At end of current row, so move to the next.
            }
        }

        private void renderTile(Tile t)
        {
            if (t == null)
            {
                Console.Write("?");
            }
            else
            {
                Console.BackgroundColor = t.Background;
                Console.ForegroundColor = t.Foreground;
                Console.Write(t.Icon);
                resetConsoleColors();
            }
        }

        private void resetConsoleColors()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
