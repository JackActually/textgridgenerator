﻿using System;
using System.Drawing;
using MapGen.Entities.Grid;
using MapGen.Entities.Tiles;

namespace MapGen.Renderers
{
    public class ImageRenderer
    {
        public void RenderGridToFile(Grid g)
        {
            Bitmap b = RenderToBitmap(g);
            b.Save("world"+DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")+".bmp");
        }

        public Bitmap RenderToBitmap(Grid g)
        {
            Bitmap b = new Bitmap(g.Width, g.Height);
            for (int i = 0; i < g.Height; i++)
            {
                for (int j = 0; j < g.Width; j++)
                {
                    if (g.Tiles[j, i] == null)
                        b.SetPixel(j, i, Color.White);
                    else
                        b.SetPixel(j, i, g.Tiles[j, i].ImageColour);
                }
            }
            return b;
        }

        public Bitmap RenderToBitmap(float[,] noiseGrid)
        {
            int width = noiseGrid.GetLength(0);
            int height = noiseGrid.GetLength(1);

            Bitmap b = new Bitmap(width, height);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (noiseGrid[j, i] == null)
                        b.SetPixel(j, i, Color.White);
                    else
                        b.SetPixel(j, i, elevationToColour(noiseGrid[j, i]));
                }
            }
            return b;
        }
        private float _sealevel;
        public Bitmap RenderToBitmap(MapGen.Entities.Grid.Region r, float sealevel)
        {
            _sealevel = sealevel;
            int width = r.elevations.GetLength(0);
            int height = r.elevations.GetLength(1);

            Bitmap b = new Bitmap(width, height);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    b.SetPixel(j, i, elevationMoistureToColor(r.elevations[j, i], r.moisture[j,i]));
                }
            }
            return b;
        }

        public Bitmap RenderGreyscaleBitmap(float[,] noiseGrid)
        {
            int width = noiseGrid.GetLength(0);
            int height = noiseGrid.GetLength(1);

            Bitmap bmp = new Bitmap(width, height);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    int calc = (int)(noiseGrid[i, j]);
                    bmp.SetPixel(i, j, Color.FromArgb(calc, calc, calc));
                }
            }
            return bmp;
        }

        private Color elevationToColour(float elevation)
        {
            Color c = new Color();
            int e = (int)Math.Abs((elevation) * 100);
            int el = (int)(255 * elevation);
            if (elevation < 0.4) // Water
            {
                // Blue
                c = Color.FromArgb(0, 0, safeColourValue((int)Math.Abs((elevation + 2) * 100)));
            }
            else if (elevation < 0.55) // Beach
            {
                c = Color.FromArgb(el, el, 50);
            }
            else if (elevation < 0.85) // Grassland
            {
                c = Color.FromArgb(255 - el, safeColourValue(255 - el + 100), 255 - el);
            }
            else if (elevation < 0.95) // Hills
            {
                c = Color.FromArgb(safeColourValue(30 - el), safeColourValue(255 - el + 60), safeColourValue(30 - el));
            }
            else if (elevation < 1.105) // Mountain
            {
                c = Color.FromArgb(safeColourValue(e), safeColourValue(e), safeColourValue(e));
            }
            else // Snowcap
            {
                c = Color.FromArgb(safeColourValue((int)(255 * elevation)), safeColourValue((int)(255 * elevation)), safeColourValue((int)(255 * elevation)));
            }
            return c;
        }

        // This needs to be replaced in time with some proper biome system.
        // Also, there have to be better ways to determine this kind of thing, surely.
        private Color elevationMoistureToColor(float elevation, float moisture)
        {
            Color c = new Color();
            if (elevation < _sealevel) // Water
            {
                // Blue
                c = Color.FromArgb(0, 0, safeColourValue((int)Math.Abs((elevation + 2) * 100)));
            }
            else if (elevation < 0.55)
            {
                if (moisture < 0.4)
                {
                    c = ColorTranslator.FromHtml("#FFE9DDC7");
                }
                else if (moisture < 0.8)
                {
                    c = ColorTranslator.FromHtml("#FFC4D4AA");
                }
                else if (moisture < 1.2)
                {
                    c = ColorTranslator.FromHtml("#FFA9CCA4");
                }
                else
                {
                    c = ColorTranslator.FromHtml("#FF9CBBA9");
                }
            }
            else if (elevation < 0.85)
            {
                if (moisture < 0.4)
                {
                    c = ColorTranslator.FromHtml("#FFE4E8CA");
                }
                else if (moisture < 0.8)
                {
                    c = ColorTranslator.FromHtml("#FFC4D4AA");
                }
                else if (moisture < 1.2)
                {
                    c = ColorTranslator.FromHtml("#FFB4C9A9");
                }
                else
                {
                    c = ColorTranslator.FromHtml("#FFA4C4A8");
                }
            }
            else if (elevation < 1.3)
            {
                if (moisture < 0.2)
                {
                    c = ColorTranslator.FromHtml("#FFE4B8CA");
                }
                else if (moisture < 1.4)
                {
                    c = ColorTranslator.FromHtml("#FFC4CCBB");
                }
                else
                {
                    c = ColorTranslator.FromHtml("#FFCCD4BB");
                }
            }
            else
            {
                if (moisture < 0.4)
                {
                    c = ColorTranslator.FromHtml("#FF999999");
                }
                else if (moisture < 0.8)
                {
                    c = ColorTranslator.FromHtml("#FFBBBBBB");
                }
                else if (moisture < 1.2)
                {
                    c = ColorTranslator.FromHtml("#FFDDDDBB");
                }
                else
                {
                    c = Color.White;
                }
            }
            return c;
        }

        //private Color elevationToColour(float elevation)
        //{
        //    Color c = new Color();
        //    int e = (int)Math.Abs((elevation) * 100);
        //    int el = (int)elevation;
        //    if (elevation < 0.4) // Water
        //    {
        //        // Blue
        //        c = Color.FromArgb(0, 0, safeColourValue((int)Math.Abs((elevation + 2) * 100)));
        //    }
        //    else if (elevation < 20) // Beach
        //    {
        //        c = Color.FromArgb(el, el, 50);
        //    }
        //    else if (elevation < 100) // Grassland
        //    {
        //        c = Color.FromArgb(255 - el, safeColourValue(255 - el + 100), 255 - el);
        //    }
        //    else if (elevation < 180) // Hills
        //    {
        //        c = Color.FromArgb(safeColourValue(30 - el), safeColourValue(255 - el + 60), safeColourValue(30 - el));
        //    }
        //    else if (elevation < 255) // Mountain
        //    {
        //        c = Color.FromArgb(safeColourValue(e), safeColourValue(e), safeColourValue(e));
        //    }
        //    else // Snowcap
        //    {
        //        c = Color.FromArgb(safeColourValue((int)(255 * elevation)), safeColourValue((int)(255 * elevation)), safeColourValue((int)(255 * elevation)));
        //    }
        //    return c;
        //}

        private int safeColourValue(int input)
        {
            if (input > 255)
                return 255;
            else if (input < 0)
                return 0;
            else
                return input;
        }
    }
}
