﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapGen.Entities.Grid;
using Newtonsoft.Json;

namespace MapGen.Storage
{
    public class RegionSerializer
    {
        public Region Deserialise(string fileName)
        {
            using (StreamReader sr = File.OpenText(fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                return (Region)serializer.Deserialize(sr, typeof(Region));
            }
        }

        public void Serialize(string fileName, Region r)
        {
            JsonSerializer serializer = new JsonSerializer();
            using (StreamWriter sw = new StreamWriter(fileName))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, r);
            }
        }
    }
}
