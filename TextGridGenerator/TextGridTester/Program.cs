﻿using System;
using MapGen.Renderers;
using MapGen.Generators;
using MapGen.Entities.Grid;

namespace MapGen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(150, 84);
            IGridGenerator gm = new SpiralGridGenerator();
            IGridGenerator pn = new SimplexGridGenerator(31417254);
            ConsoleRenderer cr = new ConsoleRenderer();
            ImageRenderer ir = new ImageRenderer();
            
            Console.WriteLine("Welcome to MapGen. Keys-\n"+
            "Q: Quit\n"+
            "1: Generate a new world.\n"+
            "2: Generate a new, non-square world.\n"+
            "3: Render a world to bitmap.\n"+
            "4: Use old-school linear generator.\n"+
            "5: Render a 2000x2000 pixel bitmap of a world generated using Simplex noise.");
            ConsoleKeyInfo input;
            while((input = Console.ReadKey()).Key != ConsoleKey.Q)
            {
                Console.WriteLine();
                Console.WriteLine();
                if (input.Key == ConsoleKey.D1)
                {
                    cr.RenderGrid(gm.GenerateGrid(75, 75));
                    Console.WriteLine("Another? Press any key except Q to generate again, press Q to quit.");
                }
                else if (input.Key == ConsoleKey.D2)
                {
                    cr.RenderGrid(gm.GenerateGrid(140, 60));
                    Console.WriteLine("Another? Press any key except Q to generate again, press Q to quit.");
                }
                else if (input.Key == ConsoleKey.D3)
                {
                    Console.WriteLine("How wide?");
                    int width = int.Parse(Console.ReadLine());
                    Console.WriteLine("How tall?");
                    int height = int.Parse(Console.ReadLine());
                    Console.WriteLine("Rendering now, hold tight...");
                    ir.RenderGridToFile(gm.GenerateGrid(width, height));
                    Console.WriteLine("Done! Check the program folder for the result.");
                    Console.WriteLine("Another? Press any key except Q to generate again, press Q to quit.");
                }
                else if (input.Key == ConsoleKey.D4)
                {
                    Grid g = gm.GenerateGrid(100, 100);
                    ir.RenderGridToFile(g);
                    cr.RenderGrid(g);
                    Console.WriteLine("Another? Press any key except Q to generate again, press Q to quit.");
                }
                else if (input.Key == ConsoleKey.D5)
                {
                    Grid g = pn.GenerateGrid(2000, 2000);
                    ir.RenderGridToFile(g);
                    Console.WriteLine("Another? Press any key except Q to generate again, press Q to quit.");
                }
                else
                {
                    Console.WriteLine("Invalid key pressed.");
                }
            }
        }
    }
}
